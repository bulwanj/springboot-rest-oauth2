package com.eteam.oauth2serversampl.dto;

import java.io.Serializable;

public class QsgOrderEntryData implements Serializable {


    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>QsgOrderEntryData.productCode</code> property defined at extension <code>qsgintegrations</code>. */

    private String productCode;

    /** <i>Generated property</i> for <code>QsgOrderEntryData.quantity</code> property defined at extension <code>qsgintegrations</code>. */

    private Integer quantity;

    public QsgOrderEntryData()
    {
        // default constructor
    }

    public void setProductCode(final String productCode)
    {
        this.productCode = productCode;
    }

    public String getProductCode()
    {
        return productCode;
    }

    public void setQuantity(final Integer quantity)
    {
        this.quantity = quantity;
    }

    public Integer getQuantity()
    {
        return quantity;
    }


}
