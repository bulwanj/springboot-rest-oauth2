package com.eteam.oauth2serversampl.dto;

import java.io.Serializable;
import java.util.List;

public class OrderHistoryRequestData implements Serializable {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>OrderHistoryRequestData.statuses</code> property defined at extension <code>qsgintegrations</code>. */

    private List<String> statuses;

    /** <i>Generated property</i> for <code>OrderHistoryRequestData.userUid</code> property defined at extension <code>qsgintegrations</code>. */

    private String userUid;

    public OrderHistoryRequestData()
    {
        // default constructor
    }

    public void setStatuses(final List<String> statuses)
    {
        this.statuses = statuses;
    }

    public List<String> getStatuses()
    {
        return statuses;
    }

    public void setUserUid(final String userUid)
    {
        this.userUid = userUid;
    }

    public String getUserUid()
    {
        return userUid;
    }
}
