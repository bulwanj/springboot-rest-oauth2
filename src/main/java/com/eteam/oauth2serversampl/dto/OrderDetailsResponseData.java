package com.eteam.oauth2serversampl.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class OrderDetailsResponseData implements Serializable {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>OrderDetailsResponseData.code</code> property defined at extension <code>qsgintegrations</code>. */

    private String code;

    /** <i>Generated property</i> for <code>OrderDetailsResponseData.ownerEmail</code> property defined at extension <code>qsgintegrations</code>. */

    private String ownerEmail;

    /** <i>Generated property</i> for <code>OrderDetailsResponseData.placed</code> property defined at extension <code>qsgintegrations</code>. */

    private Date placed;

    /** <i>Generated property</i> for <code>OrderDetailsResponseData.updated</code> property defined at extension <code>qsgintegrations</code>. */

    private Date updated;

    /** <i>Generated property</i> for <code>OrderDetailsResponseData.currencyIso</code> property defined at extension <code>qsgintegrations</code>. */

    private String currencyIso;

    /** <i>Generated property</i> for <code>OrderDetailsResponseData.value</code> property defined at extension <code>qsgintegrations</code>. */

    private BigDecimal value;

    /** <i>Generated property</i> for <code>OrderDetailsResponseData.status</code> property defined at extension <code>qsgintegrations</code>. */

    private String status;

    /** <i>Generated property</i> for <code>OrderDetailsResponseData.entries</code> property defined at extension <code>qsgintegrations</code>. */

    private List<QsgOrderEntryData> entries;

    public OrderDetailsResponseData()
    {
        // default constructor
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }

    public void setOwnerEmail(final String ownerEmail)
    {
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerEmail()
    {
        return ownerEmail;
    }

    public void setPlaced(final Date placed)
    {
        this.placed = placed;
    }

    public Date getPlaced()
    {
        return placed;
    }

    public void setUpdated(final Date updated)
    {
        this.updated = updated;
    }

    public Date getUpdated()
    {
        return updated;
    }

    public void setCurrencyIso(final String currencyIso)
    {
        this.currencyIso = currencyIso;
    }

    public String getCurrencyIso()
    {
        return currencyIso;
    }

    public void setValue(final BigDecimal value)
    {
        this.value = value;
    }

    public BigDecimal getValue()
    {
        return value;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    public void setEntries(final List<QsgOrderEntryData> entries)
    {
        this.entries = entries;
    }

    public List<QsgOrderEntryData> getEntries()
    {
        return entries;
    }

}
