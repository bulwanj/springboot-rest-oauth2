package com.eteam.oauth2serversampl.dto;

import java.io.Serializable;

public class OrderDetailsRequestData implements Serializable {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>OrderDetailsRequestData.code</code> property defined at extension <code>qsgintegrations</code>. */

    private String code;

    /** <i>Generated property</i> for <code>OrderDetailsRequestData.userUid</code> property defined at extension <code>qsgintegrations</code>. */

    private String userUid;

    public OrderDetailsRequestData()
    {
        // default constructor
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }

    public void setUserUid(final String userUid)
    {
        this.userUid = userUid;
    }

    public String getUserUid()
    {
        return userUid;
    }


}
