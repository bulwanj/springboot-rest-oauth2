package com.eteam.oauth2serversampl.dto;

import java.io.Serializable;
import java.util.List;

public class OrderHistoryResponseData implements Serializable {
    /**
     * Default serialVersionUID value.
     */

    private static final long serialVersionUID = 1L;

    /**
     * <i>Generated property</i> for <code>OrderHistoryResponseData.orders</code> property defined at extension <code>qsgintegrations</code>.
     */

    private List<QsgIntegrationOrderData> orders;

    /**
     * <i>Generated property</i> for <code>OrderHistoryResponseData.currentPage</code> property defined at extension <code>qsgintegrations</code>.
     */

    private Integer currentPage;

    /**
     * <i>Generated property</i> for <code>OrderHistoryResponseData.pageSize</code> property defined at extension <code>qsgintegrations</code>.
     */

    private Integer pageSize;

    /**
     * <i>Generated property</i> for <code>OrderHistoryResponseData.totalNumberOfResults</code> property defined at extension <code>qsgintegrations</code>.
     */

    private Integer totalNumberOfResults;

    /**
     * <i>Generated property</i> for <code>OrderHistoryResponseData.numberOfPages</code> property defined at extension <code>qsgintegrations</code>.
     */

    private Integer numberOfPages;

    public OrderHistoryResponseData() {
        // default constructor
    }

    public void setOrders(final List<QsgIntegrationOrderData> orders) {
        this.orders = orders;
    }

    public List<QsgIntegrationOrderData> getOrders() {
        return orders;
    }

    public void setCurrentPage(final Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setTotalNumberOfResults(final Integer totalNumberOfResults) {
        this.totalNumberOfResults = totalNumberOfResults;
    }

    public Integer getTotalNumberOfResults() {
        return totalNumberOfResults;
    }

    public void setNumberOfPages(final Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

}
