package com.eteam.oauth2serversampl.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class QsgIntegrationOrderData implements Serializable {

    /** Default serialVersionUID value. */

    private static final long serialVersionUID = 1L;

    /** <i>Generated property</i> for <code>QsgIntegrationOrderData.code</code> property defined at extension <code>qsgintegrations</code>. */

    private String code;

    /** <i>Generated property</i> for <code>QsgIntegrationOrderData.placed</code> property defined at extension <code>qsgintegrations</code>. */

    private Date placed;

    /** <i>Generated property</i> for <code>QsgIntegrationOrderData.currencyIso</code> property defined at extension <code>qsgintegrations</code>. */

    private String currencyIso;

    /** <i>Generated property</i> for <code>QsgIntegrationOrderData.value</code> property defined at extension <code>qsgintegrations</code>. */

    private BigDecimal value;

    /** <i>Generated property</i> for <code>QsgIntegrationOrderData.status</code> property defined at extension <code>qsgintegrations</code>. */

    private String status;

    public QsgIntegrationOrderData()
    {
        // default constructor
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }

    public void setPlaced(final Date placed)
    {
        this.placed = placed;
    }

    public Date getPlaced()
    {
        return placed;
    }

    public void setCurrencyIso(final String currencyIso)
    {
        this.currencyIso = currencyIso;
    }

    public String getCurrencyIso()
    {
        return currencyIso;
    }

    public void setValue(final BigDecimal value)
    {
        this.value = value;
    }

    public BigDecimal getValue()
    {
        return value;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

}
