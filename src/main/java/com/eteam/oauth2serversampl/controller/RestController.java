package com.eteam.oauth2serversampl.controller;

import com.eteam.oauth2serversampl.dto.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.Optional;

@Controller
public class RestController {

    @GetMapping("/user/me")
    public Principal user(Principal principal) {
        return principal;
    }

    @GetMapping("/hello")
    public ResponseEntity<String> user() {
        return ResponseEntity.ok("hello oauth2");
    }

    @PostMapping(value = "/orders")
    public ResponseEntity<OrderHistoryResponseData> orders( @RequestBody final OrderHistoryRequestData request) {

        try {

            ObjectMapper jsonMapper = new ObjectMapper();
            File jsonFile = ResourceUtils.getFile("classpath:json-objects/order-history.json");
            OrderHistoryResponseData responseDTO = jsonMapper.readValue(jsonFile, OrderHistoryResponseData.class);

            return ResponseEntity.of(Optional.of(responseDTO));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/order")
    public ResponseEntity<OrderDetailsResponseData> order(@RequestBody final OrderDetailsRequestData request) {

        try {

            ObjectMapper jsonMapper = new ObjectMapper();
            File jsonFile = ResourceUtils.getFile("classpath:json-objects/order-history.json");
            OrderHistoryResponseData responseOrdersDTO = jsonMapper.readValue(jsonFile, OrderHistoryResponseData.class);

            Optional<QsgIntegrationOrderData> orderResult = responseOrdersDTO.getOrders().stream().filter(order -> request.getCode().equalsIgnoreCase(order.getCode()))
                    .findAny();

            if (orderResult.isEmpty()) {
                return ResponseEntity.notFound().build();
            }


            QsgIntegrationOrderData orderData = orderResult.get();

            OrderDetailsResponseData responseData = new OrderDetailsResponseData();

            responseData.setCode(request.getCode());
            responseData.setOwnerEmail(request.getUserUid());

            responseData.setCurrencyIso(orderData.getCurrencyIso());
            responseData.setPlaced(orderData.getPlaced());
            responseData.setStatus(orderData.getStatus());
            responseData.setValue(orderData.getValue());


            return ResponseEntity.of(Optional.of(responseData));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.noContent().build();
    }


}
